import Company from "./components/Company";
import HrSection from "./components/HrSection";
import Finance from "./components/Finance";

export const routes = [
  {
    path: "/",
    name: "company",
    component: Company,
  },

  {
    path: "/hr",
    name: "hr_section",
    component: HrSection,
  },

  {
    path: "/finance",
    name: "finance_section",
    component: Finance,
  },
];
